# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  let(:category) { create :taxon, name: 'Categories' }
  let!(:leaf_category) { create :taxon, name: 'Leaf', parent_id: category.id }
  let!(:other_category) { create :taxon, name: 'Other' }

  let(:product) { create :base_product }
  let(:other_product) { create :base_product }
  let!(:product_taxon) do
    Spree::Classification.create(
      product_id: product.id,
      taxon_id: category.id
    )
  end
  let!(:other_product_taxon) do
    Spree::Classification.create(
      product_id: other_product.id,
      taxon_id: other_category.id
    )
  end

  describe 'GET #show' do
    context '正常系' do
      it 'showテンプレートを表示すること' do
        get :show, id: category.id
        expect(response).to render_template :show
      end

      it '@categoryに要求されたidのtaxonが割り当てられていること' do
        get :show, id: category.id
        expect(assigns(:category)).to eq category
      end

      it '@productsに要求した商品が割り当てられること' do
        get :show, id: category.id
        expect(assigns(:products)).to include product
      end

      it '@category_taxonsにcategoryの子孫が割り当てられていること' do
        get :show, id: category.id
        expect(assigns(:category_taxons)).to include leaf_category
      end
    end
  end

  context '異常系' do
    it '@productsにcategoryをtaxonに持つ商品以外は割り当てられていないこと' do
      get :show, id: category.id
      expect(assigns(:products)).not_to include other_product
    end

    it '@category_taxonsにother_categoryの子孫は割り当てられていないこと' do
      get :show, id: category.id
      expect(assigns(:category_taxons)).not_to include other_category
    end
  end
end
