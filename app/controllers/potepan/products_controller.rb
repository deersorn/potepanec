class Potepan::ProductsController < ApplicationController
  def show
    @product = Spree::Product.first
    @related_products = Spree::Taxon.find(@product.taxons.second).products.where.not(id: @product.id).limit(4)
  end
end
