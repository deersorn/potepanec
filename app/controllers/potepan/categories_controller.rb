class Potepan::CategoriesController < ApplicationController
  def show
    @category = Spree::Taxon.find(params[:id])
    @products = @category.products
    # サイドバー
    @category_taxons = Spree::Taxon.find_by(name: 'Categories').leaves
  end
end

